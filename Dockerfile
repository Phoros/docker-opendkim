FROM alpine:latest

# Install dependencies
RUN apk add --update --no-cache --no-progress bash rsyslog su-exec opendkim opendkim-utils

# Copy the configuration
COPY ./config/etc/opendkim/opendkim.conf /etc/opendkim/opendkim.conf 

# Copy provisioning assets
COPY ./assets/docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["/sbin/su-exec", "opendkim:opendkim", "/usr/sbin/opendkim", "-f", "-x", "/etc/opendkim/opendkim.conf"]
