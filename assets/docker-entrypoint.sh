#!/bin/bash
set -eo pipefail
shopt -s nullglob

error() {
    echo >&2 "! Error on or near line ${1}"                                                                                                                                                                                                 exit 1
}
trap 'error ${LINENO}' ERR

# Environment checks
if [ -z "$MAIL_DOMAINS" ]; then
        echo >&2 "ERROR: No mail domains defined"
        exit 1
fi

if [ -z "$REPORT_MAIL_ADDRESS" ]; then
        echo >&2 "ERROR: No mail address defined for DKIM reports"
        exit 1
fi

SELECTOR=${SELECTOR:-default}

if [ ! -f /initialized ]; then
    # Create dedicated run folder for OpenDKIM
    if [ ! -d /run/opendkim/ ]; then
        mkdir /run/opendkim/
        chown -R opendkim:opendkim /run/opendkim/
        chmod -R 0770 /run/opendkim/
    fi

    # Change report address in config file
    sed "s/postmaster@example.org/${REPORT_MAIL_ADDRESS}/" -i /etc/opendkim/opendkim.conf

    touch /etc/opendkim/TrustedHosts
    chmod 644 /etc/opendkim/TrustedHosts
    for host in 127.0.0.1 ::1 localhost $MAIL_DOMAINS; do
        echo "$host" >> /etc/opendkim/TrustedHosts
    done

    # Set permissions after all changes are done in /etc/opendkim/
    chown -R opendkim:opendkim /etc/opendkim/
    chmod -R a=r,u+w,a+X /etc/opendkim/

    # Create keys directory when it doesn't exist yet
    if [ ! -d /var/opendkim/keys/ ]; then
        mkdir -p /var/opendkim/keys/
    fi

    # Create key pairs for domains
    for domain in $MAIL_DOMAINS; do
        if [ ! -f "/var/opendkim/keys/$domain/${SELECTOR}.private" ]; then
            if [ ! -d "/var/opendkim/keys/$domain/" ]; then
                mkdir -p /var/opendkim/keys/$domain/
            fi

            opendkim-genkey --subdomains --domain=${domain} --selector=${SELECTOR} --bits=2048 --directory=/var/opendkim/keys/${domain}/
        fi
        echo "${SELECTOR}._domainkey.${domain} ${domain}:${SELECTOR}:/var/opendkim/keys/${domain}/${SELECTOR}.private" >> /var/opendkim/KeyTable
        echo "*@${domain} ${SELECTOR}._domainkey.${domain}" >> /var/opendkim/SigningTable
    done

    echo "### DKIM DNS RECORDS"
    for domain in $MAIL_DOMAINS; do
        echo "---------- $domain"
        cat /var/opendkim/keys/$domain/${SELECTOR}.txt
        echo "----------"
    done
    echo "### END OF DKIM DNS RECORDS"
fi

# Fix permissions
chown -R opendkim:opendkim /var/opendkim/
find /var/opendkim/ -type d -exec chmod 750 {} \;
find /var/opendkim/ -type f -exec chmod 600 {} \;

# Fix Rsyslog configuration (comment out the kernel module line)
sed -E -i /etc/rsyslog.conf -e 's/(\$ModLoad imklog.so.*$)/#\1/'

# Set symlink for the OpenDKIM log file
ln -sf /dev/stdout /var/log/maillog

# Start the daemon
/usr/sbin/rsyslogd

exec "$@"
